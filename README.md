# Python 3 Virtualenv

# Install Python 3
$ sudo apt-get install python3

# Install python3-virtualenv
$ sudo apt-get install python3-virtualenv

# You can checkout you virtualenv version
$ virtualenv --version

# Create you virtualenv in the folder you that want to
$ cd /my/path/to/my/venv/folder/container
$ virtualenv --python=/your/path/to/python3 your/virtualenv/folder/name
# Example
$ cd /myprojects
$ virtualenv --python=/usr/bin/python3.7 venv

# Check if the folder was created
$ ls

# Activate your virtualenv
$ source venv/bin/activate

# Check yuo python version on this virtualenv
python --version

# Check yuo pip version on this virtualenv
$ pip --version

# When you finish working, you can deacgtivate your virtualenv
$ deavtivate
